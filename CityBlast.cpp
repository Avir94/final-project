////////////////////////////////////////////////////////////////////////
//
//   Westmont College
//   CS150 : 3D Computer Graphics
//   Professor David Hunter
//
//   Code derived from book code for _Foundations of 3D Computer Graphics_
//   by Steven Gortler.  See AUTHORS file for more details.
//
////////////////////////////////////////////////////////////////////////

#include <string>
#include <vector>
#include <string>
#include <memory>
#include <stdexcept>
#if __GNUG__
#   include <tr1/memory>
#endif

#include <GL/glew.h>
#ifdef __MAC__
#   include <GLUT/glut.h>
#else
#   include <GL/glut.h>
#endif

#include "cvec.h"
#include "matrix4.h"
#include "geometrymaker.h"
#include "ppm.h"
#include "glsupport.h"
#include "quat.h"
#include "quatrbt.h"
#include "city.h"
#include "bezpath.h"

using namespace std; // for string, vector, iostream, and other standard C++ stuff
using namespace tr1; // for shared_ptr

// G L O B A L S ///////////////////////////////////////////////////

// --------- IMPORTANT --------------------------------------------------------
// Before you start working on this assignment, set the following variable
// properly to indicate whether you want to use OpenGL 2.x with GLSL 1.0 or
// OpenGL 3.x+ with GLSL 1.3.
//
// Set g_Gl2Compatible = true to use GLSL 1.0 and g_Gl2Compatible = false to
// use GLSL 1.3. Make sure that your machine supports the version of GLSL you
// are using. In particular, on Mac OS X currently there is no way of using
// OpenGL 3.x with GLSL 1.3 when GLUT is used.
//
// If g_Gl2Compatible=true, shaders with -gl2 suffix will be loaded.
// If g_Gl2Compatible=false, shaders with -gl3 suffix will be loaded.
// To complete the assignment you only need to edit the shader files that get
// loaded
// ----------------------------------------------------------------------------
static const bool g_Gl2Compatible = false;

static const float g_frustMinFov = 60.0;  // A minimal of 60 degree field of view
static float g_frustFovY = g_frustMinFov; // FOV in y direction (updated by updateFrustFovY)

static const float g_frustNear = -0.1;    // near plane
static const float g_frustFar = -50.0;    // far plane
static const float g_groundY = -2.0;      // y coordinate of the ground
static const float g_groundSize = 50.0;   // half the ground length

static int g_windowWidth = 800;
static int g_windowHeight = 800;
static bool g_mouseClickDown = false;    // is the mouse button pressed
static bool g_mouseLClickButton, g_mouseRClickButton, g_mouseMClickButton;
static int g_mouseClickX, g_mouseClickY; // coordinates for mouse click event
static int object = 0; // the object the user is controlling (0 = left cube, 1 = right cube, 2 = sky-view)
static int ssws = 0; // either sky-sky or world-sky

// Animation globals for time-based animation
static const float g_animStart = 0;
static const float g_animMax = 39;                      // animClock maximum for bezier animation
static const float g_animMaxB = .5;                     // animClock maximum for Bullet animation
static float g_animClock = g_animStart;                // clock parameter runs from g_animStart to g_animMax then repeats
static float g_animClockB = g_animStart;               // clock parameter runs from g_animStart to g_animMaxB
static float g_animSpeed = 0.125;                      // clock units per second
static float g_animSpeedB = 1;                         // clock units per second
static int g_elapsedTime = 0;                          // keeps track of how long it takes between frames
static float g_animIncrement = g_animSpeed/60.0;       // updated by idle() based on GPU speed
static float g_animIncrementB = g_animSpeed/60.0;      // updated by idleB() based on GPU speed
static int view = 0;                                   // which view to look from. 0 = first-person, 1 = easeIn
static bool shootBullet = false;                       // determines whether a bullet has been shot or not, only 1 bullet at a time
static bool enemy = false;                             // determines whether an enemy is present or not 
static bool developerMode = true;                     // toggles developer mode
static int score = 0;                                  // Point collection during gameplay
static bool* keyStates = new bool[256];                // Create an array of boolean values of length 256 (0-255) will be used for keyboard buffering 

struct ShaderState {
  GlProgram program;

  // Handles to uniform variables
  GLint h_uLight;
  GLint h_uProjMatrix;
  GLint h_uModelViewMatrix;
  GLint h_uNormalMatrix;
  GLint h_uColor;
  GLint h_uTexUnit0; 
  GLint h_uColorBool;
  GLint h_uTexUnitBool;
  GLint h_uViewBool;

  // Handles to vertex attributes
  GLint h_aPosition;
  GLint h_aNormal;
  GLint h_aTexCoord0;

  ShaderState(const char* vsfn, const char* fsfn) {
    readAndCompileShader(program, vsfn, fsfn);

    const GLuint h = program; // short hand

    // Retrieve handles to uniform variables
    h_uLight = safe_glGetUniformLocation(h, "uLight");
    h_uProjMatrix = safe_glGetUniformLocation(h, "uProjMatrix");
    h_uModelViewMatrix = safe_glGetUniformLocation(h, "uModelViewMatrix");
    h_uNormalMatrix = safe_glGetUniformLocation(h, "uNormalMatrix");
    h_uColor = safe_glGetUniformLocation(h, "uColor");
    h_uTexUnit0 = safe_glGetUniformLocation(h, "uTexUnit0"); 
    h_uColorBool = safe_glGetUniformLocation(h, "uColorBool");
    h_uTexUnitBool = safe_glGetUniformLocation(h, "uTexUnitBool");
    h_uViewBool = safe_glGetUniformLocation(h, "uViewBool");

    // Retrieve handles to vertex attributes
    h_aPosition = safe_glGetAttribLocation(h, "aPosition");
    h_aNormal = safe_glGetAttribLocation(h, "aNormal");
    h_aTexCoord0 = safe_glGetAttribLocation(h, "aTexCoord0"); 

    if (!g_Gl2Compatible)
      glBindFragDataLocation(h, 0, "fragColor");
    checkGlErrors();
  }

};

static const int g_numShaders = 3;
static const char * const g_shaderFiles[g_numShaders][3] = {
  {"./shaders/basic-gl3.vshader", "./shaders/textured-gl3.fshader"},
  {"./shaders/basic-gl3.vshader", "./shaders/solid-gl3.fshader"},
  {"./shaders/basic-gl3.vshader", "./shaders/phong-gl3.fshader"}
};
static const char * const g_shaderFilesGl2[g_numShaders][3] = {
  {"./shaders/basic-gl2.vshader", "./shaders/textured-gl2.fshader"}, 
  {"./shaders/basic-gl2.vshader", "./shaders/solid-gl2.fshader"},
  {"./shaders/basic-gl2.vshader", "./shaders/phong-gl2.fshader"}
};

static vector<shared_ptr<ShaderState> > g_shaderStates; // our global shader state

static shared_ptr<GlTexture> g_tex0, g_tex1, g_tex2; // our global texture instance

// --------- Geometry

// Macro used to obtain relative offset of a field within a struct
#define FIELD_OFFSET(StructType, field) &(((StructType *)0)->field)

// A vertex with floating point Position, Normal, and one set of teXture coordinates;
struct VertexPNX {
  Cvec3f p, n; // position and normal vectors
  Cvec2f x; // texture coordinates

  VertexPNX() {}

  VertexPNX(float x, float y, float z,
            float nx, float ny, float nz,
            float u, float v)
    : p(x,y,z), n(nx, ny, nz), x(u, v) 
  {}

  VertexPNX(const Cvec3f& pos, const Cvec3f& normal, const Cvec2f& texCoords)
    :  p(pos), n(normal), x(texCoords) {}

  VertexPNX(const Cvec3& pos, const Cvec3& normal, const Cvec2& texCoords)
    : p(pos[0], pos[1], pos[2]), n(normal[0], normal[1], normal[2]), x(texCoords[0], texCoords[1]) {}

  // Define copy constructor and assignment operator from GenericVertex so we can
  // use make* functions from geometrymaker.h
  VertexPNX(const GenericVertex& v) {
    *this = v;
  }

  VertexPNX& operator = (const GenericVertex& v) {
    p = v.pos;
    n = v.normal;
    x = v.tex;
    return *this;
  }
};

struct Geometry {
  GlBufferObject vbo, ibo;
  int vboLen, iboLen;

  Geometry(VertexPNX *vtx, unsigned short *idx, int vboLen, int iboLen) {
    this->vboLen = vboLen;
    this->iboLen = iboLen;

    // Now create the VBO and IBO
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(VertexPNX) * vboLen, vtx, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned short) * iboLen, idx, GL_STATIC_DRAW);
  }

  void draw(const ShaderState& curSS) {
    // Enable the attributes used by our shader
    safe_glEnableVertexAttribArray(curSS.h_aPosition);
    safe_glEnableVertexAttribArray(curSS.h_aNormal);
    safe_glEnableVertexAttribArray(curSS.h_aTexCoord0); 

    // bind vbo
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    safe_glVertexAttribPointer(curSS.h_aPosition, 3, GL_FLOAT, GL_FALSE, sizeof(VertexPNX), FIELD_OFFSET(VertexPNX, p));
    safe_glVertexAttribPointer(curSS.h_aNormal, 3, GL_FLOAT, GL_FALSE, sizeof(VertexPNX), FIELD_OFFSET(VertexPNX, n));
    safe_glVertexAttribPointer(curSS.h_aTexCoord0, 2, GL_FLOAT, GL_FALSE, sizeof(VertexPNX), FIELD_OFFSET(VertexPNX, x));

    // bind ibo
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

    // draw!
    glDrawElements(GL_TRIANGLES, iboLen, GL_UNSIGNED_SHORT, 0);

    // Disable the attributes used by our shader
    safe_glDisableVertexAttribArray(curSS.h_aPosition);
    safe_glDisableVertexAttribArray(curSS.h_aNormal);
    safe_glDisableVertexAttribArray(curSS.h_aTexCoord0); 
  }
};

// Vertex buffer and index buffer associated with the ground and cube geometry
static shared_ptr<Geometry> g_ground, g_cube, g_sphere, g_octahedron, g_tube, g_cone;

// --------- Scene

static Matrix4 g_lightRbt = Matrix4::makeTranslation(Cvec3(0,30,0));
static QuatRBT g_skyRbt = QuatRBT(Cvec3(-5, -1.7, -3));
static Matrix4 g_easeInRbt = Matrix4::makeTranslation(Cvec3(5, 2, -3));
static Matrix4 g_topDownRbt = Matrix4::makeTranslation(Cvec3(0,4,0));
static Matrix4 g_objectRbt[2] = {Matrix4::makeTranslation(Cvec3(-1,0,-8)), Matrix4::makeTranslation(Cvec3(0,0.25,0))};  // currently only 2 objs are defined
static Cvec3f g_objectColors[2] = {Cvec3f(1, 0, 0)};

///////////////// END OF G L O B A L S //////////////////////////////////////////////////

static void initGround() {
  // A x-z plane at y = g_groundY of dimension [-g_groundSize, g_groundSize]^2
  VertexPNX vtx[4] = {
    VertexPNX(-g_groundSize, g_groundY, -g_groundSize, 0, 1, 0, 0, 0), 
    VertexPNX(-g_groundSize, g_groundY,  g_groundSize, 0, 1, 0, 0, 1), 
    VertexPNX( g_groundSize, g_groundY,  g_groundSize, 0, 1, 0, 1, 1),
    VertexPNX( g_groundSize, g_groundY, -g_groundSize, 0, 1, 0, 1, 0),
  };
  unsigned short idx[] = {0, 1, 2, 0, 2, 3};
  g_ground.reset(new Geometry(&vtx[0], &idx[0], 4, 6));
}

static void initObjects() {
  int ibLen, vbLen;
  getCubeVbIbLen(vbLen, ibLen);

  // Temporary storage for cube geometry
  vector<VertexPNX> vtx(vbLen);
  vector<unsigned short> idx(ibLen);

  makeCube(1, vtx.begin(), idx.begin());
  makeCube(1, vtx.begin(), idx.begin());
  g_cube.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen));

  getSphereVbIbLen(50, 50, vbLen, ibLen);
  vtx.resize(vbLen);
  idx.resize(ibLen);
  makeSphere(.01, 50, 50, vtx.begin(), idx.begin());
  g_sphere.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen));

  getOctahedronVbIbLen(vbLen, ibLen);
  vtx.resize(vbLen);
  idx.resize(ibLen);
  makeOctahedron(.1, vtx.begin(), idx.begin());
  g_octahedron.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen));  

  getTubeVbIbLen(50, vbLen, ibLen);
  vtx.resize(vbLen);
  idx.resize(ibLen);
  makeTube(.5, 1, 50, vtx.begin(), idx.begin());
  g_tube.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen)); 

  getConeVbIbLen(50, vbLen, ibLen);
  vtx.resize(vbLen);
  idx.resize(ibLen);
  makeCone(.75, 1, 50, vtx.begin(), idx.begin());
  g_cone.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen));
}


// takes a projection matrix and send to the the shaders
static void sendProjectionMatrix(const ShaderState& curSS, const Matrix4& projMatrix) {
  GLfloat glmatrix[16];
  projMatrix.writeToColumnMajorMatrix(glmatrix); // send projection matrix
  safe_glUniformMatrix4fv(curSS.h_uProjMatrix, glmatrix);
}

// takes model view matrix to the shaders
static void sendModelViewMatrix(const ShaderState& curSS, const Matrix4& MVM) {
  GLfloat glmatrix[16];
  MVM.writeToColumnMajorMatrix(glmatrix); // send MVM
  safe_glUniformMatrix4fv(curSS.h_uModelViewMatrix, glmatrix);
}

// takes MVM and its normal matrix to the shaders
static void sendModelViewNormalMatrix(const ShaderState& SS, const Matrix4& MVM, const Matrix4& NMVM) {
  GLfloat glmatrix[16];
  MVM.writeToColumnMajorMatrix(glmatrix); // send MVM
  safe_glUniformMatrix4fv(SS.h_uModelViewMatrix, glmatrix);

  NMVM.writeToColumnMajorMatrix(glmatrix); // send NMVM
  safe_glUniformMatrix4fv(SS.h_uNormalMatrix, glmatrix);
}


// update g_frustFovY from g_frustMinFov, g_windowWidth, and g_windowHeight
static void updateFrustFovY() {
  if (g_windowWidth >= g_windowHeight)
    g_frustFovY = g_frustMinFov;
  else {
    const double RAD_PER_DEG = 0.5 * CS150_PI/180;
    g_frustFovY = atan2(sin(g_frustMinFov * RAD_PER_DEG) * g_windowHeight / g_windowWidth, cos(g_frustMinFov * RAD_PER_DEG)) / RAD_PER_DEG;
  }
}

static Matrix4 makeProjectionMatrix() {
  return Matrix4::makeProjection(
           g_frustFovY, g_windowWidth / static_cast <double> (g_windowHeight),
           g_frustNear, g_frustFar);
}

static Matrix4 easeIn(Matrix4 camera, Matrix4 target, Cvec3 distance, double alpha){
  Cvec3 ctrans = Cvec3(camera(0,3), camera(1,3), camera(2,3));
  Cvec3 ttrans = Cvec3(target(0,3) + distance[0], target(1,3) + distance[1], target(2,3) + distance[2]);
  Cvec3 newt = Cvec3(ctrans * (1 - alpha) + (ttrans * alpha));
  Cvec3 z = normalize(ctrans - ttrans);
  Cvec3 x = normalize(cross(Cvec3(0,1,0), z));
  Cvec3 y = cross(z,x);

  for(int i=0; i<3; i++){
    for(int j=0; j<3; j++){
      if(i==0)
	camera(j,i) = x[j];
      if(i==1)
	camera(j,i) = y[j];
      if(i==2)
	camera(j,i) = z[j];
    }
  }

  camera(0,3) = newt[0];
  camera(1,3) = newt[1];
  camera(2,3) = newt[2];
  
  return camera;
}

static int makeRand(int num){
  num = rand() % 100-50;
  if(num < 25 && num > -25)
    makeRand(num);
  return num;
}

static void drawScene() {
  // short hand for current shader state
  const ShaderState& curSS = *g_shaderStates[2];
  const ShaderState& solidSS = *g_shaderStates[1];
  if(!enemy && !developerMode){
    int e = rand() % 1000;
    if(e < 10){
      enemy = true;
      cout << "an enemy is approaching!"
	   << endl;
    }
  }

  // build & send proj. matrix to vshader
  const Matrix4 projmat = makeProjectionMatrix();
  sendProjectionMatrix(curSS, projmat);

  if(!developerMode){
    QuatRBT position = getBez(g_animClock);             // Animate the camera along the bezier path
    g_skyRbt.setTranslation(position.getTranslation()); // Allows for user to rotate camera
  }
  
  g_topDownRbt = rigTFormToMatrix(g_skyRbt) * Matrix4::makeTranslation(Cvec3(0,4,0.01));
  Cvec3 z = normalize(Cvec3(g_topDownRbt(0,3), g_topDownRbt(1,3), g_topDownRbt(2,3)) - g_skyRbt.getTranslation());
  Cvec3 x = normalize(cross(Cvec3(0,1,0), z));
  Cvec3 y = cross(z, x);

  for(int i=0; i<3; i++){
    for(int j=0; j<3; j++){
      if(i==0)
	g_topDownRbt(j,i) = x[j];
      if(i==1)
	g_topDownRbt(j,i) = y[j];
      if(i==2)
	g_topDownRbt(j,i) = z[j];
    }
  }

  // use the skyRbt as the eyeRbt
  Matrix4 eyeRbt;
  if(view==0){
    eyeRbt = rigTFormToMatrix(g_skyRbt);
    safe_glUniform1f(curSS.h_uViewBool, true);
    if(!developerMode)
      g_easeInRbt = easeIn(g_easeInRbt, rigTFormToMatrix(g_skyRbt), Cvec3(0,.5,0), 0.01);
  }
  if(view==1){
    eyeRbt = g_easeInRbt;
    safe_glUniform1f(curSS.h_uViewBool, false);
    if(!developerMode)
      g_easeInRbt = easeIn(g_easeInRbt, rigTFormToMatrix(g_skyRbt), Cvec3(0,.5,0), 0.01);
  } 
  if(view==2){
    eyeRbt = g_topDownRbt;
    safe_glUniform1f(curSS.h_uViewBool, false);
    if(!developerMode)
      g_easeInRbt = easeIn(g_easeInRbt, rigTFormToMatrix(g_skyRbt), Cvec3(0,.5,0), 0.01);
  }

  const Matrix4 invEyeRbt = inv(eyeRbt);
  const Cvec3 lCoords= Cvec3(g_lightRbt(0,3),g_lightRbt(1,3),g_lightRbt(2,3));
  safe_glUniform3f(curSS.h_uLight, lCoords[0], lCoords[1], lCoords[2]); // shaders need light positions  

  Matrix4 taller;
  taller(1,1) = 3;
  
  // Draw Ground
  // ===========
  //
  const QuatRBT groundRbt = QuatRBT();  // identity
  Matrix4 MVM = invEyeRbt * rigTFormToMatrix(groundRbt);
  Matrix4 NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 0, 0, 0); // set color
  safe_glUniform1f(curSS.h_uColorBool, true);
  safe_glUniform1i(curSS.h_uTexUnit0, 0); // texture unit 0 for ground
  safe_glUniform1f(curSS.h_uTexUnitBool, true);
  g_ground->draw(curSS);

  // Draw Sun
  // ==========
  //
  glUseProgram(curSS.program);          // select shader we want to use
  sendProjectionMatrix(curSS, projmat); // send projection matrix to shader
  MVM = invEyeRbt * g_lightRbt * Matrix4::makeScale(2);
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 1.0, .5, 0.0);
  safe_glUniform1i(curSS.h_uTexUnit0, 2);
  safe_glUniform1f(curSS.h_uColorBool, true);
  safe_glUniform1f(curSS.h_uTexUnitBool, true);
  g_sphere->draw(curSS);

  // Draw Player
  // ==========
  // Head
  MVM = invEyeRbt * rigTFormToMatrix(g_skyRbt) * Matrix4::makeTranslation(Cvec3(0,.15,0)) * Matrix4::makeScale(10);
  NMVM = normalMatrix(MVM);
  if(view==1)
    glDisable(GL_CULL_FACE);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, .8, .6, .2);
  safe_glUniform1f(curSS.h_uColorBool, true);
  safe_glUniform1f(curSS.h_uViewBool, true);
  safe_glUniform1f(curSS.h_uTexUnitBool, false);
  g_sphere->draw(curSS);
  glEnable(GL_CULL_FACE);
  safe_glUniform1f(curSS.h_uViewBool, false);

  // Eyes
  MVM = invEyeRbt * rigTFormToMatrix(g_skyRbt) * Matrix4::makeTranslation(Cvec3(-.04,.165,-.075)) * Matrix4::makeScale(3);
  NMVM = normalMatrix(MVM);
  if(view==1)
    glDisable(GL_CULL_FACE);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 0, 0, 0);
  safe_glUniform1f(curSS.h_uColorBool, true);
  safe_glUniform1f(curSS.h_uViewBool, true);
  safe_glUniform1f(curSS.h_uTexUnitBool, false);
  g_sphere->draw(curSS);
  glEnable(GL_CULL_FACE);
  safe_glUniform1f(curSS.h_uViewBool, false);

  MVM = invEyeRbt * rigTFormToMatrix(g_skyRbt) * Matrix4::makeTranslation(Cvec3(.04,.165,-.075)) * Matrix4::makeScale(3);
  NMVM = normalMatrix(MVM);
  if(view==1)
    glDisable(GL_CULL_FACE);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 0, 0, 0);
  safe_glUniform1f(curSS.h_uColorBool, true);
  safe_glUniform1f(curSS.h_uViewBool, true);
  safe_glUniform1f(curSS.h_uTexUnitBool, false);
  g_sphere->draw(curSS);
  glEnable(GL_CULL_FACE);
  safe_glUniform1f(curSS.h_uViewBool, false);

  // Torso
  MVM = invEyeRbt * rigTFormToMatrix(g_skyRbt) * taller * Matrix4::makeTranslation(Cvec3(0,0,0)) * Matrix4::makeScale(.09) * Matrix4::makeXRotation(90);
  NMVM = normalMatrix(MVM);
  if(view==1)
    glDisable(GL_CULL_FACE);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 1, 1, 1);
  safe_glUniform1f(curSS.h_uColorBool, true);
  safe_glUniform1f(curSS.h_uViewBool, true);
  safe_glUniform1f(curSS.h_uTexUnitBool, false);
  g_tube->draw(curSS);
  glEnable(GL_CULL_FACE);
  safe_glUniform1f(curSS.h_uViewBool, false);

  // Left Arm
  //   Sleeve
  MVM = invEyeRbt * rigTFormToMatrix(g_skyRbt) * Matrix4::makeTranslation(Cvec3(-.025,0.022,-0.025)) * Matrix4::makeScale(.06) * Matrix4::makeZRotation(60) * Matrix4::makeYRotation(45);
  NMVM = normalMatrix(MVM);
  if(view==1)
    glDisable(GL_CULL_FACE);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 1, 1, 1);
  safe_glUniform1f(curSS.h_uColorBool, true);
  safe_glUniform1f(curSS.h_uViewBool, true);
  safe_glUniform1f(curSS.h_uTexUnitBool, false);
  g_tube->draw(curSS);
  glEnable(GL_CULL_FACE);
  safe_glUniform1f(curSS.h_uViewBool, false);
  
  //   Arm
  MVM = invEyeRbt * rigTFormToMatrix(g_skyRbt) * Matrix4::makeTranslation(Cvec3(-.035,0.005,-0.05)) * Matrix4::makeScale(.05) * Matrix4::makeZRotation(60) * Matrix4::makeYRotation(45);
  NMVM = normalMatrix(MVM);
  if(view==1)
    glDisable(GL_CULL_FACE);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, .8, .6, .2);
  safe_glUniform1f(curSS.h_uColorBool, true);
  safe_glUniform1f(curSS.h_uViewBool, true);
  safe_glUniform1f(curSS.h_uTexUnitBool, false);
  g_tube->draw(curSS);
  glEnable(GL_CULL_FACE);
  safe_glUniform1f(curSS.h_uViewBool, false);

  //   Hand
  MVM = invEyeRbt * rigTFormToMatrix(g_skyRbt) * Matrix4::makeTranslation(Cvec3(-.045,-0.015,-0.075)) * Matrix4::makeScale(3);
  NMVM = normalMatrix(MVM);
  if(view==1)
    glDisable(GL_CULL_FACE);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, .8, .6, .2);
  safe_glUniform1f(curSS.h_uColorBool, true);
  safe_glUniform1f(curSS.h_uViewBool, true);
  safe_glUniform1f(curSS.h_uTexUnitBool, false);
  g_sphere->draw(curSS);
  glEnable(GL_CULL_FACE);
  safe_glUniform1f(curSS.h_uViewBool, false);

  //   Gun
  //     Handle
  MVM = invEyeRbt * rigTFormToMatrix(g_skyRbt) * Matrix4::makeTranslation(Cvec3(-.045,0.015,-0.085)) * Matrix4::makeScale(.03) * Matrix4::makeXRotation(90);
  NMVM = normalMatrix(MVM);
  if(view==1)
    glDisable(GL_CULL_FACE);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 0, 0, 0);
  safe_glUniform1f(curSS.h_uColorBool, true);
  safe_glUniform1f(curSS.h_uViewBool, true);
  safe_glUniform1f(curSS.h_uTexUnitBool, false);
  g_tube->draw(curSS);
  glEnable(GL_CULL_FACE);
  safe_glUniform1f(curSS.h_uViewBool, false);

  //     barrel
  if(view!=0){
    MVM = invEyeRbt * rigTFormToMatrix(g_skyRbt) * Matrix4::makeTranslation(Cvec3(-.045,0.015,-0.115)) * Matrix4::makeXRotation(90) * taller * Matrix4::makeScale(.03) * Matrix4::makeXRotation(-90);
    NMVM = normalMatrix(MVM);
    if(view==1)
      glDisable(GL_CULL_FACE);
    sendModelViewNormalMatrix(curSS, MVM, NMVM);
    safe_glUniform3f(curSS.h_uColor, 0, 0, 0);
    safe_glUniform1f(curSS.h_uColorBool, true);
    safe_glUniform1f(curSS.h_uViewBool, true);
    safe_glUniform1f(curSS.h_uTexUnitBool, false);
    g_tube->draw(curSS);
    glEnable(GL_CULL_FACE);
    safe_glUniform1f(curSS.h_uViewBool, false);
  }

  // Right Arm
  //   Sleeve
  MVM = invEyeRbt * rigTFormToMatrix(g_skyRbt) * Matrix4::makeTranslation(Cvec3(.025,0.022,-0.025)) * Matrix4::makeScale(.06) * Matrix4::makeZRotation(-60) * Matrix4::makeYRotation(-45);
  NMVM = normalMatrix(MVM);
  if(view==1)
    glDisable(GL_CULL_FACE);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 1, 1, 1);
  safe_glUniform1f(curSS.h_uColorBool, true);
  safe_glUniform1f(curSS.h_uViewBool, true);
  safe_glUniform1f(curSS.h_uTexUnitBool, false);
  g_tube->draw(curSS);
  glEnable(GL_CULL_FACE);
  safe_glUniform1f(curSS.h_uViewBool, false);

  //   Arm
  MVM = invEyeRbt * rigTFormToMatrix(g_skyRbt) * Matrix4::makeTranslation(Cvec3(.035,0.005,-0.05)) * Matrix4::makeScale(.05) * Matrix4::makeZRotation(-60) * Matrix4::makeYRotation(-45);
  NMVM = normalMatrix(MVM);
  if(view==1)
    glDisable(GL_CULL_FACE);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, .8, .6, .2);
  safe_glUniform1f(curSS.h_uColorBool, true);
  safe_glUniform1f(curSS.h_uViewBool, true);
  safe_glUniform1f(curSS.h_uTexUnitBool, false);
  g_tube->draw(curSS);
  glEnable(GL_CULL_FACE);
  safe_glUniform1f(curSS.h_uViewBool, false);

  //   Hand
  MVM = invEyeRbt * rigTFormToMatrix(g_skyRbt) * Matrix4::makeTranslation(Cvec3(.045,-0.015,-0.075)) * Matrix4::makeScale(3);
  NMVM = normalMatrix(MVM);
  if(view==1)
    glDisable(GL_CULL_FACE);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, .8, .6, .2);
  safe_glUniform1f(curSS.h_uColorBool, true);
  safe_glUniform1f(curSS.h_uViewBool, true);
  safe_glUniform1f(curSS.h_uTexUnitBool, false);
  g_sphere->draw(curSS);
  glEnable(GL_CULL_FACE);
  safe_glUniform1f(curSS.h_uViewBool, false);

  //   Gun
  //     Handle
  MVM = invEyeRbt * rigTFormToMatrix(g_skyRbt) * Matrix4::makeTranslation(Cvec3(.045,0.015,-0.085)) * Matrix4::makeScale(.03) * Matrix4::makeXRotation(90);
  NMVM = normalMatrix(MVM);
  if(view==1)
    glDisable(GL_CULL_FACE);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 0, 0, 0);
  safe_glUniform1f(curSS.h_uColorBool, true);
  safe_glUniform1f(curSS.h_uViewBool, true);
  safe_glUniform1f(curSS.h_uTexUnitBool, false);
  g_tube->draw(curSS);
  glEnable(GL_CULL_FACE);
  safe_glUniform1f(curSS.h_uViewBool, false);

  //     barrel
  if(view!=0){
    MVM = invEyeRbt * rigTFormToMatrix(g_skyRbt) * Matrix4::makeTranslation(Cvec3(.045,0.015,-0.115)) * Matrix4::makeXRotation(90) * taller * Matrix4::makeScale(.03) * Matrix4::makeXRotation(-90);
    NMVM = normalMatrix(MVM);
    if(view==1)
      glDisable(GL_CULL_FACE);
    sendModelViewNormalMatrix(curSS, MVM, NMVM);
    safe_glUniform3f(curSS.h_uColor, 0, 0, 0);
    safe_glUniform1f(curSS.h_uColorBool, true);
    safe_glUniform1f(curSS.h_uViewBool, true);
    safe_glUniform1f(curSS.h_uTexUnitBool, false);
    g_tube->draw(curSS);
    glEnable(GL_CULL_FACE);
    safe_glUniform1f(curSS.h_uViewBool, false);
  }

  // Bottom
  Matrix4 flatten;
  flatten(1,1) = .375;
  MVM = invEyeRbt * rigTFormToMatrix(g_skyRbt) * Matrix4::makeTranslation(Cvec3(0,-.15,0)) * Matrix4::makeScale(15) * flatten;
  NMVM = normalMatrix(MVM);
  if(view==1)
    glDisable(GL_CULL_FACE);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 1, .2, .2);
  safe_glUniform1f(curSS.h_uColorBool, true);
  safe_glUniform1f(curSS.h_uViewBool, true);
  safe_glUniform1f(curSS.h_uTexUnitBool, false);
  g_sphere->draw(curSS);
  glEnable(GL_CULL_FACE);
  safe_glUniform1f(curSS.h_uViewBool, false);

  // Draw City
  // ========== Cube 1
  Building* g_myCity;                       // all building (cubes) are defined in the city.cpp file 
  for(int i=0; i<39; i++){                  // iterate through size of how many buildings in the scene
    g_myCity = getCity(i);                  // size of how many buildings in the scene/in the city.cpp file 
    MVM = invEyeRbt * rigTFormToMatrix(g_myCity[i].xyz) * Matrix4::makeScale(g_myCity[i].s) * taller;
    NMVM = normalMatrix(MVM);
    sendModelViewNormalMatrix(curSS, MVM, NMVM);
    safe_glUniform3f(curSS.h_uColor, g_objectColors[0][1], g_objectColors[0][1], g_objectColors[0][2]);
    safe_glUniform1i(curSS.h_uTexUnit0, 1); // texture unit 1 for cube
    safe_glUniform1f(curSS.h_uColorBool, true);
    safe_glUniform1f(curSS.h_uTexUnitBool, true);
    g_cube->draw(curSS);
  }

  // Draw Trees
  // ==========
  /*
  glUseProgram(solidSS.program);
  MVM = rigTFormToMatrix(invEyeRbt * g_objectRbt[0]) * Matrix4::makeTranslation(Cvec3(12,1,0)) * Matrix4::makeXRotation(90);
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(solidSS, MVM, NMVM);
  safe_glUniform3f(solidSS.h_uColor, .5, .5, 0); 
  glDisable(GL_CULL_FACE);
  g_tube->draw(solidSS);
  glUseProgram(curSS.program);
  glEnable(GL_CULL_FACE);
  */

  if(!shootBullet)
    g_objectRbt[0] = rigTFormToMatrix(g_skyRbt);
  if(g_mouseLClickButton)
   shootBullet=true;


  // Draw Bullet
  // ===========
  if(shootBullet){
    g_objectRbt[0] = g_objectRbt[0] * Matrix4::makeTranslation(Cvec3(0.0125,0,-g_animClockB*4));
    MVM = invEyeRbt * g_objectRbt[0] * Matrix4::makeScale(.25);
    NMVM = normalMatrix(MVM);
    g_objectRbt[0] = g_objectRbt[0] * Matrix4::makeTranslation(Cvec3(-0.0125,0,-g_animClockB*4));
    sendModelViewNormalMatrix(curSS, MVM, NMVM);
    safe_glUniform3f(curSS.h_uColor, 0, 0, 0.0);
    safe_glUniform1f(curSS.h_uColorBool, true);
    safe_glUniform1f(curSS.h_uTexUnitBool, false);
    g_sphere->draw(curSS);

    g_objectRbt[0] = g_objectRbt[0] * Matrix4::makeTranslation(Cvec3(-0.0125,0,-g_animClockB*4));
    MVM = invEyeRbt * g_objectRbt[0] * Matrix4::makeScale(.25);
    NMVM = normalMatrix(MVM);
    g_objectRbt[0] = g_objectRbt[0] * Matrix4::makeTranslation(Cvec3(0.0125,0,-g_animClockB*4));
    sendModelViewNormalMatrix(curSS, MVM, NMVM);
    safe_glUniform3f(curSS.h_uColor, 0, 0, 0.0);
    safe_glUniform1f(curSS.h_uColorBool, true);
    safe_glUniform1f(curSS.h_uTexUnitBool, false);
    g_sphere->draw(curSS);
  }

  if(!enemy){
    int randx = makeRand(randx);
    int randz = rand() % 25-50;
    g_objectRbt[1] = rigTFormToMatrix(g_skyRbt) * Matrix4::makeTranslation(Cvec3(randx, 1, randz));
  }
    
  // Draw Enemy
  // ==========
  if(enemy && !developerMode){
    g_objectRbt[1] = easeIn(g_objectRbt[1], rigTFormToMatrix(g_skyRbt), Cvec3(0,0,0), 0.065);
    MVM = invEyeRbt * g_objectRbt[1]; // * Matrix4::makeScale(.1);
    NMVM = normalMatrix(MVM);
    sendModelViewNormalMatrix(curSS, MVM, NMVM);
    safe_glUniform3f(curSS.h_uColor, 0, .9, .85);
    safe_glUniform1f(curSS.h_uColorBool, true);
    safe_glUniform1f(curSS.h_uViewBool, true);
    safe_glUniform1f(curSS.h_uTexUnitBool, false);
    g_octahedron->draw(curSS);

    MVM = invEyeRbt * g_objectRbt[1] * Matrix4::makeTranslation(Cvec3(0.0275,0.0275,-0.05)) * Matrix4::makeXRotation(90) * taller * Matrix4::makeScale(.03) * Matrix4::makeXRotation(-90);
    NMVM = normalMatrix(MVM);
    sendModelViewNormalMatrix(curSS, MVM, NMVM);
    safe_glUniform3f(curSS.h_uColor, 0, 0, 0);
    safe_glUniform1f(curSS.h_uColorBool, true);
    safe_glUniform1f(curSS.h_uViewBool, true);
    safe_glUniform1f(curSS.h_uTexUnitBool, false);
    g_cone->draw(curSS);

    MVM = invEyeRbt * g_objectRbt[1] * Matrix4::makeTranslation(Cvec3(-0.0275,0.0275,-0.05)) * Matrix4::makeXRotation(90) * taller * Matrix4::makeScale(.03) * Matrix4::makeXRotation(-90);
    NMVM = normalMatrix(MVM);
    sendModelViewNormalMatrix(curSS, MVM, NMVM);
    safe_glUniform3f(curSS.h_uColor, 0, 0, 0);
    safe_glUniform1f(curSS.h_uColorBool, true);
    safe_glUniform1f(curSS.h_uViewBool, true);
    safe_glUniform1f(curSS.h_uTexUnitBool, false);
    g_cone->draw(curSS);

    MVM = invEyeRbt * g_objectRbt[1] * Matrix4::makeTranslation(Cvec3(0.0275,-0.0275,-0.05)) * Matrix4::makeXRotation(90) * taller * Matrix4::makeScale(.03) * Matrix4::makeXRotation(-90);
    NMVM = normalMatrix(MVM);
    sendModelViewNormalMatrix(curSS, MVM, NMVM);
    safe_glUniform3f(curSS.h_uColor, 0, 0, 0);
    safe_glUniform1f(curSS.h_uColorBool, true);
    safe_glUniform1f(curSS.h_uViewBool, true);
    safe_glUniform1f(curSS.h_uTexUnitBool, false);
    g_cone->draw(curSS);

    MVM = invEyeRbt * g_objectRbt[1] * Matrix4::makeTranslation(Cvec3(-0.0275,-0.0275,-0.05)) * Matrix4::makeXRotation(90) * taller * Matrix4::makeScale(.03) * Matrix4::makeXRotation(-90);
    NMVM = normalMatrix(MVM);
    sendModelViewNormalMatrix(curSS, MVM, NMVM);
    safe_glUniform3f(curSS.h_uColor, 0, 0, 0);
    safe_glUniform1f(curSS.h_uColorBool, true);
    safe_glUniform1f(curSS.h_uViewBool, true);
    safe_glUniform1f(curSS.h_uTexUnitBool, false);
    g_cone->draw(curSS);

    MVM = invEyeRbt * g_objectRbt[1] * Matrix4::makeXRotation(90) * taller * Matrix4::makeScale(.075) * Matrix4::makeTranslation(Cvec3(0,0.5,0)) * Matrix4::makeXRotation(-90);
    NMVM = normalMatrix(MVM);
    sendModelViewNormalMatrix(curSS, MVM, NMVM);
    safe_glUniform3f(curSS.h_uColor, 1, 0, 1);
    safe_glUniform1f(curSS.h_uColorBool, true);
    safe_glUniform1f(curSS.h_uViewBool, true);
    safe_glUniform1f(curSS.h_uTexUnitBool, false);
    g_tube->draw(curSS);

    MVM = invEyeRbt * g_objectRbt[1] * Matrix4::makeXRotation(90) * Matrix4::makeScale(.2) * flatten * Matrix4::makeTranslation(Cvec3(0,3,0)) * Matrix4::makeXRotation(-90) * Matrix4::makeZRotation(g_animClock * 180);
    NMVM = normalMatrix(MVM);
    sendModelViewNormalMatrix(curSS, MVM, NMVM);
    safe_glUniform3f(curSS.h_uColor, 0, 1, 0);
    safe_glUniform1f(curSS.h_uColorBool, true);
    safe_glUniform1f(curSS.h_uViewBool, true);
    safe_glUniform1f(curSS.h_uTexUnitBool, false);
    g_cube->draw(curSS);
    safe_glUniform1f(curSS.h_uViewBool, false);

    double xp = g_skyRbt.getTranslation()[0] - g_objectRbt[1](0,3);
    double yp = g_skyRbt.getTranslation()[1] - g_objectRbt[1](1,3);
    double zp = g_skyRbt.getTranslation()[2] - g_objectRbt[1](2,3);
    if(abs(xp)< .22 && abs(yp) < .22 && abs(zp) < .22){
      cout << "You died!"
	   << "Final Score: "
	   << score
	   << endl;
      glutIdleFunc(NULL);  					  // idle callback for animation
      enemy = false;
      developerMode = true;
      glutPostRedisplay();
    }
  }

  if(shootBullet && enemy){
    double xs = g_objectRbt[0](0,3) - g_objectRbt[1](0,3);
    double ys = g_objectRbt[0](1,3) - g_objectRbt[1](1,3);
    double zs = g_objectRbt[0](2,3) - g_objectRbt[1](2,3);
    if(abs(xs) < .4 && abs(ys) < .2 && abs(zs) < .2){
      shootBullet = false;
      enemy = false;
      score += 1000;
      cout << "Killed an enemy!"
	   << endl;
    }       
  }
}

static void motion(const int x, const int y) {
  const double dx = x - g_mouseClickX;
  const double dy = g_windowHeight - y - 1 - g_mouseClickY;

  QuatRBT m;
  /*
  if (g_mouseLClickButton && !g_mouseRClickButton) { // left button down?
    shootBullet = true;
  }
  */
  if(true){
    m =  QuatRBT(Quat::makeYRotation(-dx));
  }
  else if (g_mouseRClickButton && !g_mouseLClickButton) { // right button down?
    m = QuatRBT(Cvec3(dx, dy, 0) * -0.01);
  }
  else if (g_mouseMClickButton || (g_mouseLClickButton && g_mouseRClickButton)) {  // middle or (left and right) button down?
    m = QuatRBT(Cvec3(0, 0, -dy) * 0.01);
  }

  if (/*g_mouseClickDown*/true) {
    g_skyRbt = g_skyRbt * m * inv(g_skyRbt) * g_skyRbt;
    glutPostRedisplay(); // we always redraw if we changed the scene
  }

  g_mouseClickX = x;
  g_mouseClickY = g_windowHeight - y - 1;
}


static void mouse(const int button, const int state, const int x, const int y) {
  g_mouseClickX = x;
  g_mouseClickY = g_windowHeight - y - 1;  // conversion from GLUT window-coordinate-system to OpenGL window-coordinate-system
  

  g_mouseLClickButton |= (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN);
  g_mouseRClickButton |= (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN);
  g_mouseMClickButton |= (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN);

  g_mouseLClickButton &= !(button == GLUT_LEFT_BUTTON && state == GLUT_UP);
  g_mouseRClickButton &= !(button == GLUT_RIGHT_BUTTON && state == GLUT_UP);
  g_mouseMClickButton &= !(button == GLUT_MIDDLE_BUTTON && state == GLUT_UP);

  g_mouseClickDown = g_mouseLClickButton || g_mouseRClickButton || g_mouseMClickButton;
}

 static void idle(){
  g_animIncrement = g_animSpeed * g_elapsedTime / 1000; // rescale animation increment
  g_animClock += g_animIncrement;                       // Update animation clock 
  //cout << "Time: " << g_animClock << endl;
  if(g_animClock > 24 && g_animClock < 25)
    g_animClock = g_animClock + 1;
  if (g_animClock > g_animMax){                         // and cycle to start if necessary.
    g_animClock = g_animStart;
  }
  glutMotionFunc(motion);                               // mouse movement callback
  if(shootBullet){
    g_animIncrementB = g_animSpeedB * g_elapsedTime / 1000; // rescale animation increment
    g_animClockB += g_animIncrementB;                       // Update animation clock 
    if (g_animClockB > g_animMaxB){                         // and cycle to start if necessary.
      g_animClockB = g_animStart;
      shootBullet = false;
    }
  }
  glutPostRedisplay();  // for animation
}

static void keyboardOperations(){
  if(keyStates[27])
    exit(0);                                  // ESC
  if(keyStates['h']){
    cout << " ============== H E L P ==============\n\n"
    << "h\t\thelp menu\n"
    << "p\t\tsave screenshot\n"
    << "v\t\tCycles view between first person, third-person, and Top down\n"
    << "a\t\tmoves camera left if in developer mode\n"
    << "w\t\tmoves camera forward if in developer mode\n"
    << "s\t\tmoves camera backwards in in developer mode\n"
    << "d\t\tmoves camera right if in developer mode\n"
    << "space\t\tshoots a bullet\n"
    << "+\t\tincreases the speed of the game/animation\n"
    << "-\t\tdecreases the speed of the game/animation\n"
    << "y\t\ttoggles developer mode\n"
    << "r\t\trestarts game\n"
    << "drag left mouse to rotate character\n" 
    << "drag middle mouse to translate in/out if in developer mode \n" 
    << "drag right mouse to translate up/down/left/right if in developer mode\n"
    << endl;
    keyStates['h'] = false;
  }
  if(keyStates['w'])
    g_skyRbt = g_skyRbt * QuatRBT(Cvec3(0,0,-0.125));
  if(keyStates['a'])
    g_skyRbt = g_skyRbt * QuatRBT(Cvec3(-0.0625,0,0));
  if(keyStates['s'])
    g_skyRbt = g_skyRbt * QuatRBT(Cvec3(0,0,0.125));
  if(keyStates['d'])
    g_skyRbt = g_skyRbt * QuatRBT(Cvec3(0.0625,0,0));
  if(keyStates['e'])
    g_skyRbt = g_skyRbt * QuatRBT(Cvec3(0,0.125,0));			 // Prototype for jumping
  if(keyStates['q'])
    if(g_skyRbt.getTranslation()[1] > -1.75)
      g_skyRbt = g_skyRbt * QuatRBT(Cvec3(0,-0.125,0));			
  if(keyStates['v']){
    if(view==2)
      view=0;
    else
      view++;
    keyStates['v'] = false;
  }
  if(keyStates['r']){
    enemy = false;
    g_animClock = g_animStart;
    score = 0;
    glutIdleFunc(idle);  					  // idle callback for animation
    developerMode = false;
    keyStates['r']=false;
  }
  if(keyStates['+']){
    g_animSpeed *= 1.05;
  }
  if(keyStates['-']){
    g_animSpeed *= 0.95;
  }
  if(keyStates['y']){
    if(!developerMode)
      developerMode = true;
    else
      developerMode = false;
    keyStates['y']=false;
  }
  if(keyStates['p']){
    glFlush();
    writePpmScreenshot(g_windowWidth, g_windowHeight, "out.ppm");
    cout << "Screenshot written to out.ppm." << endl;
    keyStates['p']=false;
  }
}

static void display() {
  glUseProgram(g_shaderStates[2]->program);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);                   // clear framebuffer color&depth

  keyboardOperations();

  drawScene();

  glutSwapBuffers();                                    // show the back buffer (where we rendered stuff)

  checkGlErrors();


  // Calculate frames per second 
  static int oldTime = -1;
  static int frames = 0;
  static int lastTime = -1;
  
  int currentTime = glutGet(GLUT_ELAPSED_TIME); // returns milliseconds
  g_elapsedTime = currentTime - lastTime;       // how long last frame took
  lastTime = currentTime;
  
  if (oldTime < 0)
    oldTime = currentTime;
  
  frames++;
  
  if (currentTime - oldTime >= 5000) // report FPS every 5 seconds
    {
      cout << "Frames per second: "
	   << float(frames)*1000.0/(currentTime - oldTime) << endl;
      cout << "Elapsed ms since last frame: " << g_elapsedTime << endl;
      oldTime = currentTime;
      score += 100;
      cout << "Score: " << score << endl;
      frames = 0;
    }
  
}

static void reshape(const int w, const int h) {
  g_windowWidth = w;
  g_windowHeight = h;
  glViewport(0, 0, w, h);
  updateFrustFovY();
  glutPostRedisplay();
}

static void keyboardDown(const unsigned char key, const int x, const int y) {
  keyStates[key] = true; // Set the state of the current key to pressed  
  glutPostRedisplay();
}

static void keyboardUp(const unsigned char key, const int x, const int y) {
  keyStates[key] = false; // Set the state of the current key to not pressed  
}

static void initGlutState(int argc, char * argv[]) {
  glutInit(&argc, argv);                                  // initialize Glut based on cmd-line args
  glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH);  //  RGBA pixel channels and double buffering
  glutInitWindowSize(g_windowWidth, g_windowHeight);      // create a window
  glutCreateWindow("CityBlast");        // title the window

  glutDisplayFunc(display);                               // display rendering callback
  glutReshapeFunc(reshape);                               // window reshape callback
  glutPassiveMotionFunc(motion);
  glutMouseFunc(mouse);                                   // mouse click callback
  glutIdleFunc(idle);  					  // idle callback for animation
  glutKeyboardFunc(keyboardDown);
  glutKeyboardUpFunc(keyboardUp);
  glutSetCursor(GLUT_CURSOR_NONE);
}

static void initGLState() {
  glClearColor(128./255., 200./255., 255./255., 0.);
  glClearDepth(0.);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  glCullFace(GL_BACK);
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_GREATER);
  glReadBuffer(GL_BACK);
  if (!g_Gl2Compatible)
    glEnable(GL_FRAMEBUFFER_SRGB);
}

static void initShaders() {
  g_shaderStates.resize(g_numShaders);
  for (int i = 0; i < g_numShaders; ++i) {
    if (g_Gl2Compatible)
      g_shaderStates[i].reset(new ShaderState(g_shaderFilesGl2[i][0], g_shaderFilesGl2[i][1]));
    else
      g_shaderStates[i].reset(new ShaderState(g_shaderFiles[i][0], g_shaderFiles[i][1]));
  }
}

static void initGeometry() {
  initGround();
  initObjects();
}

static void loadTexture(GLuint texHandle, const char *ppmFilename) {
  int texWidth, texHeight;
  vector<PackedPixel> pixData;

  ppmRead(ppmFilename, texWidth, texHeight, pixData);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, texHandle);
  glTexImage2D(GL_TEXTURE_2D, 0, g_Gl2Compatible ? GL_RGB : GL_SRGB, texWidth, texHeight,
               0, GL_RGB, GL_UNSIGNED_BYTE, &pixData[0]);
  checkGlErrors();
}

static void initTextures() {
  g_tex0.reset(new GlTexture());
  g_tex1.reset(new GlTexture());
  g_tex2.reset(new GlTexture());

  loadTexture(*g_tex0, "tdcity.ppm");
  loadTexture(*g_tex1, "skyscraper.ppm");
  loadTexture(*g_tex2, "sun.ppm");

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, *g_tex0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, *g_tex1);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

  
  glActiveTexture(GL_TEXTURE2);
  glBindTexture(GL_TEXTURE_2D, *g_tex2);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
  
}

int main(int argc, char * argv[]) {
  try {
    initGlutState(argc,argv);

    glewInit(); // load the OpenGL extensions

    cout << (g_Gl2Compatible ? "Will use OpenGL 2.x / GLSL 1.0" : "Will use OpenGL 3.x / GLSL 1.3") << endl;
    if ((!g_Gl2Compatible) && !GLEW_VERSION_3_0)
      throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.3");
    else if (g_Gl2Compatible && !GLEW_VERSION_2_0)
      throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.0");

    initGLState();
    initShaders();
    initGeometry();
    initTextures();

    glutMainLoop();
    return 0;
  }
  catch (const runtime_error& e) {
    cout << "Exception caught: " << e.what() << endl;
    return -1;
  }
}
