#ifndef BEZPATH_H
#define BEZPATH_H

#include "quatrbt.h"

// return array of control points
QuatRBT* getCtrls(int size);

QuatRBT getBez(double t);

#endif
