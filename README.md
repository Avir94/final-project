# CS-150, Final Project
## Connor Riva

---

## Context
My project will combine the elements of almost everything we have done this year into a single project. This project will be like a small shooter video game. The "scenery" of the game will be a city made up of rectagular prisms (elongated cubes) to represent buildings sticking up from the ground. The buildings will have textures on them to make them look like skyscrapers. There will be a "character" in which the user will play as. This Character will travel along a set path as determined by a piecewise bezier curve throughout the city. As the character is moving, the player will have control of the orientation of the charcter and abke to manipulate which way the character is facing. As the character is moving, random "enemies" will pop up and "chase" the character. The objective is that the user will face the enemy and "shoot" at it. There will be a collision detection between the "bullet" and the enemy in which the enemy will be destroyed once a colision occurs. The enemies will be following the character in a ease in camera type of way. The user will have the option of being able to view the game progress in both 1st or 3rd person view. If an enemy touches the character, the character has "died" and the program will quit.

## Required components
1. **Transformations represented by QuatRBT.** Enemies, bullets, character, scenery will use QuatRBT's
2. **Keyframing.** The character will be keyframed with bezier curves. The enemies will be keyframed with ease-in camera motion. And the bullets will be keyframes using just a simple direction translation as described by the orientation of the character at the moment of firing.
3. **Ease-in camera.** The character will be the only object to have a ease-in camera.

## Other Features
1. Multiple shaders, including phong lighting and textures.
2. (Possibly) Homemade shadows using projection matrices.
3. Animation of position using an animation increment, as in Project #4.
4. Animation of position using a parametric function of the animation clock, as in Project #6.
5. Interactivity using the mouse and/or keyboard.
6. Tubes, octahedrons, or some other geometry object that you make yourself.
7. Multiple points of view.
8. A help menu that explains all the functionality.
9. Collision Detection

If I do choose to substitute any of the above "Other features" it would be substituting "Homemade shadows using projection matrices" for collision detection. I explain how this collision detection will work in the "Context" protion of this README. But, I will try to implement as much as possible into this project.