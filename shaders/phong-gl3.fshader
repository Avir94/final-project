#version 130
uniform vec3 uLight, uColor; 
uniform sampler2D uTexUnit0;
uniform bool uColorBool, uTexUnitBool, uViewBool;
 
in vec3 vNormal; // normal to surface
in vec3 vPosition; // position of point on surface
in vec2 vTexCoord0;
 
out vec4 fragColor;
 
void main() {
  vec4 texColor0 = texture(uTexUnit0, vTexCoord0);

  vec3 toLight = uLight - vec3(vPosition);
  vec3 toV = -normalize(vec3(vPosition));
  toLight = normalize(toLight);
  vec3 h = normalize(toV + toLight);
  vec3 normal = normalize(vNormal);

  float specular = 0;
  float diffuse;
  if(gl_FrontFacing == false){
    if(uViewBool)
      specular = pow(max(0.0, dot(h, -normal)), 64.0);
      diffuse = max(0.1, dot(-normal, toLight));
  }
  else{
    if(uViewBool)
      specular = pow(max(0.0, dot(h, normal)), 64.0);
      diffuse = max(0.5, dot(normal, toLight));
  }
  vec3 intensity = vec3(1,1,1);
  if(uColorBool && uTexUnitBool)
    intensity = vec3(0.1,0.1,0.1) + uColor + vec3(texColor0) * diffuse + vec3(0.6,0.6,0.6) * specular;
  else if(uColorBool)
    intensity = vec3(0.1,0.1,0.1) + uColor * diffuse + vec3(0.6,0.6,0.6) * specular;
  else
    intensity = vec3(0.1,0.1,0.1) + vec3(texColor0) * diffuse + vec3(0.6,0.6,0.6) * specular;
  fragColor = vec4(intensity.x, intensity.y, intensity.z, 1.0);
}
