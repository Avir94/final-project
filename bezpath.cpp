#include "bezpath.h"

QuatRBT* getCtrls(int size){
  QuatRBT myPath[size];
  // Curve 1
  myPath[0] = QuatRBT(Cvec3(4, -1.7, -3));
  myPath[1] = QuatRBT(Cvec3(0, -1.7, -1));
  myPath[2] = QuatRBT(Cvec3(-8, -1.7, -5));
  myPath[3] = QuatRBT(Cvec3(-15, -1.7, -3));
  // Curve 2
  myPath[4] = QuatRBT(Cvec3(-15, -1.7, -3));
  myPath[5] = QuatRBT(Cvec3(-22, -1.7, -1));
  myPath[6] = QuatRBT(Cvec3(-24, -1.7, -15));
  myPath[7] = QuatRBT(Cvec3(-23, -1.7, -20));
  // Curve 3
  myPath[8] = QuatRBT(Cvec3(-23, -1.7, -20));
  myPath[9] = QuatRBT(Cvec3(-22, -1.7, -25));
  myPath[10] = QuatRBT(Cvec3(-23, -1.7, -35));
  myPath[11] = QuatRBT(Cvec3(-23, -1.7, -39));
  // Curve 4
  myPath[12] = QuatRBT(Cvec3(-23, -1.7, -39));
  myPath[13] = QuatRBT(Cvec3(-23, -1.7, -43));
  myPath[14] = QuatRBT(Cvec3(-19, -1.7, -40));
  myPath[15] = QuatRBT(Cvec3(-11, -1.7, -40));
  // Curve 5
  myPath[16] = QuatRBT(Cvec3(-11, -1.7, -40));
  myPath[17] = QuatRBT(Cvec3(-3, -1.7, -40));
  myPath[18] = QuatRBT(Cvec3(5, -1.7, -40));
  myPath[19] = QuatRBT(Cvec3(7, -1.7, -40));
  // Curve 6
  myPath[20] = QuatRBT(Cvec3(7, -1.7, -40));
  myPath[21] = QuatRBT(Cvec3(9, -1.7, -40));
  myPath[22] = QuatRBT(Cvec3(10, -1.7, -35));
  myPath[23] = QuatRBT(Cvec3(10, -1.7, -30));
  // Curve 7
  myPath[24] = QuatRBT(Cvec3(10, -1.7, -30));
  myPath[25] = QuatRBT(Cvec3(10, -1.7, -25));
  myPath[26] = QuatRBT(Cvec3(10, -1.7, -15));
  myPath[27] = QuatRBT(Cvec3(11, -1.7, -7));
  // Curve 8
  myPath[28] = QuatRBT(Cvec3(11, -1.7, -7));
  myPath[29] = QuatRBT(Cvec3(12, -1.7, 1));
  myPath[30] = QuatRBT(Cvec3(11, -1.7, 7));
  myPath[31] = QuatRBT(Cvec3(10, -1.7, 14));
  // Curve 9
  myPath[32] = QuatRBT(Cvec3(10, -1.7, 14));
  myPath[33] = QuatRBT(Cvec3(9, -1.7, 21));
  myPath[34] = QuatRBT(Cvec3(11, -1.7, 28));
  myPath[35] = QuatRBT(Cvec3(11, -1.7, 35));
  // Curve 10
  myPath[36] = QuatRBT(Cvec3(11, -1.7, 35));
  myPath[37] = QuatRBT(Cvec3(11, -1.7, 42));
  myPath[38] = QuatRBT(Cvec3(10, -1.7, 42));
  myPath[39] = QuatRBT(Cvec3(11, -1.7, 42));
  // Curve 11
  myPath[40] = QuatRBT(Cvec3(11, -1.7, 42));
  myPath[41] = QuatRBT(Cvec3(12, -1.7, 42));
  myPath[42] = QuatRBT(Cvec3(20, -1.7, 40));
  myPath[43] = QuatRBT(Cvec3(27, -1.7, 40));
  // Curve 12  
  myPath[44] = QuatRBT(Cvec3(27, -1.7, 40));
  myPath[45] = QuatRBT(Cvec3(34, -1.7, 40));
  myPath[46] = QuatRBT(Cvec3(40, -1.7, 40));
  myPath[47] = QuatRBT(Cvec3(42, -1.7, 41));
  // Curve 13  
  myPath[48] = QuatRBT(Cvec3(42, -1.7, 41));
  myPath[49] = QuatRBT(Cvec3(44, -1.7, 42));
  myPath[50] = QuatRBT(Cvec3(43, -1.7, 37));
  myPath[51] = QuatRBT(Cvec3(43, -1.7, 30));
  // Curve 14  
  myPath[52] = QuatRBT(Cvec3(43, -1.7, 30));
  myPath[53] = QuatRBT(Cvec3(43, -1.7, 23));
  myPath[54] = QuatRBT(Cvec3(43, -1.7, 17));
  myPath[55] = QuatRBT(Cvec3(40, -1.7, 16));
  // Curve 15  
  myPath[56] = QuatRBT(Cvec3(40, -1.7, 16));
  myPath[57] = QuatRBT(Cvec3(37, -1.7, 15));
  myPath[58] = QuatRBT(Cvec3(32, -1.7, 14));
  myPath[59] = QuatRBT(Cvec3(27, -1.7, 13));
  // Curve 16  
  myPath[60] = QuatRBT(Cvec3(27, -1.7, 13));
  myPath[61] = QuatRBT(Cvec3(22, -1.7, 12));
  myPath[62] = QuatRBT(Cvec3(18, -1.7, 12));
  myPath[63] = QuatRBT(Cvec3(14, -1.7, 13));
  // Curve 17  
  myPath[64] = QuatRBT(Cvec3(14, -1.7, 13));
  myPath[65] = QuatRBT(Cvec3(10, -1.7, 14));
  myPath[66] = QuatRBT(Cvec3(10, -1.7, 20));
  myPath[67] = QuatRBT(Cvec3(10, -1.7, 25));
  // Curve 18  
  myPath[68] = QuatRBT(Cvec3(10, -1.7, 25));
  myPath[69] = QuatRBT(Cvec3(10, -1.7, 30));
  myPath[70] = QuatRBT(Cvec3(9, -1.7, 38));
  myPath[71] = QuatRBT(Cvec3(9, -1.7, 40));
  // Curve 19  
  myPath[72] = QuatRBT(Cvec3(9, -1.7, 40));
  myPath[73] = QuatRBT(Cvec3(9, -1.7, 42));
  myPath[74] = QuatRBT(Cvec3(3, -1.7, 40));
  myPath[75] = QuatRBT(Cvec3(-4, -1.7, 40));
  // Curve 20  
  myPath[76] = QuatRBT(Cvec3(-4, -1.7, 40));
  myPath[77] = QuatRBT(Cvec3(-11, -1.7, 40));
  myPath[78] = QuatRBT(Cvec3(-13, -1.7, 41));
  myPath[79] = QuatRBT(Cvec3(-15, -1.7, 40));
  // Curve 21  
  myPath[80] = QuatRBT(Cvec3(-15, -1.7, 40));
  myPath[81] = QuatRBT(Cvec3(-17, -1.7, 39));
  myPath[82] = QuatRBT(Cvec3(-23, -1.7, 40));
  myPath[83] = QuatRBT(Cvec3(-27, -1.7, 40));
  // Curve 22  
  myPath[84] = QuatRBT(Cvec3(-27, -1.7, 40));
  myPath[85] = QuatRBT(Cvec3(-31, -1.7, 40));
  myPath[86] = QuatRBT(Cvec3(-37, -1.7, 42));
  myPath[87] = QuatRBT(Cvec3(-40, -1.7, 40));
  // Curve 23  
  myPath[88] = QuatRBT(Cvec3(-40, -1.7, 40));
  myPath[89] = QuatRBT(Cvec3(-43, -1.7, 38));
  myPath[90] = QuatRBT(Cvec3(-44, -1.7, 35));
  myPath[91] = QuatRBT(Cvec3(-44, -1.7, 29));
  // Curve 24  
  myPath[92] = QuatRBT(Cvec3(-44, -1.7, 29));
  myPath[93] = QuatRBT(Cvec3(-44, -1.7, 24));
  myPath[94] = QuatRBT(Cvec3(-25, -1.7, 27));
  myPath[95] = QuatRBT(Cvec3(-24, -1.7, 27));
  // Curve 25 Wierd glitch at time 24-25, so im jumping over
  myPath[96] = QuatRBT(Cvec3(-32, -1.7, 27));
  myPath[97] = QuatRBT(Cvec3(-26, -1.7, 27));
  myPath[98] = QuatRBT(Cvec3(-25, -1.7, 27));
  myPath[99] = QuatRBT(Cvec3(-24, -1.7, 27));
  // Curve 26
  myPath[100] = QuatRBT(Cvec3(-24, -1.7, 27));
  myPath[101] = QuatRBT(Cvec3(-23, -1.7, 27));
  myPath[102] = QuatRBT(Cvec3(-23, -1.7, 21));
  myPath[103] = QuatRBT(Cvec3(-23, -1.7, 16));
  // Curve 27
  myPath[104] = QuatRBT(Cvec3(-23, -1.7, 16));
  myPath[105] = QuatRBT(Cvec3(-23, -1.7, 11));
  myPath[106] = QuatRBT(Cvec3(-23, -1.7, 7));
  myPath[107] = QuatRBT(Cvec3(-20, -1.7, 6));
  // Curve 28
  myPath[108] = QuatRBT(Cvec3(-20, -1.7, 6));
  myPath[109] = QuatRBT(Cvec3(-17, -1.7, 5));
  myPath[110] = QuatRBT(Cvec3(-12, -1.7, 5));
  myPath[111] = QuatRBT(Cvec3(-8, -1.7, 5));
  // Curve 29
  myPath[112] = QuatRBT(Cvec3(-8, -1.7, 5));
  myPath[113] = QuatRBT(Cvec3(-4, -1.7, 5));
  myPath[114] = QuatRBT(Cvec3(-3, -1.7, 0));
  myPath[115] = QuatRBT(Cvec3(-2, -1.7, -3));
  // Curve 30
  myPath[116] = QuatRBT(Cvec3(-2, -1.7, -3));
  myPath[117] = QuatRBT(Cvec3(-1, -1.7, -6));//
  myPath[118] = QuatRBT(Cvec3(5, -1.7, -6));
  myPath[119] = QuatRBT(Cvec3(10, -1.7, -5));
  // Curve 31
  myPath[120] = QuatRBT(Cvec3(10, -1.7, -5));
  myPath[121] = QuatRBT(Cvec3(15, -1.7, -4));
  myPath[122] = QuatRBT(Cvec3(20, -1.7, -5));
  myPath[123] = QuatRBT(Cvec3(25, -1.7, -5));
  // Curve 32
  myPath[124] = QuatRBT(Cvec3(25, -1.7, -5));
  myPath[125] = QuatRBT(Cvec3(30, -1.7, -5));
  myPath[126] = QuatRBT(Cvec3(31, -1.7, -7));
  myPath[127] = QuatRBT(Cvec3(32, -1.7, -10));
  // Curve 33
  myPath[128] = QuatRBT(Cvec3(32, -1.7, -10));
  myPath[129] = QuatRBT(Cvec3(32, -1.7, -13));
  myPath[130] = QuatRBT(Cvec3(31, -1.7, -19));
  myPath[131] = QuatRBT(Cvec3(31, -1.7, -24));
  // Curve 34
  myPath[132] = QuatRBT(Cvec3(31, -1.7, -24));
  myPath[133] = QuatRBT(Cvec3(31, -1.7, -29));
  myPath[134] = QuatRBT(Cvec3(30, -1.7, -34));
  myPath[135] = QuatRBT(Cvec3(30, -1.7, -36));
  // Curve 35
  myPath[136] = QuatRBT(Cvec3(30, -1.7, -36));
  myPath[137] = QuatRBT(Cvec3(30, -1.7, -38));//
  myPath[138] = QuatRBT(Cvec3(31, -1.7, -40));
  myPath[139] = QuatRBT(Cvec3(37, -1.7, -40));
  // Curve 36
  myPath[140] = QuatRBT(Cvec3(37, -1.7, -40));
  myPath[141] = QuatRBT(Cvec3(43, -1.7, -40));//
  myPath[142] = QuatRBT(Cvec3(43, -1.7, -34));
  myPath[143] = QuatRBT(Cvec3(44, -1.7, -29));
  // Curve 37
  myPath[144] = QuatRBT(Cvec3(44, -1.7, -29));
  myPath[145] = QuatRBT(Cvec3(45, -1.7, -24));
  myPath[146] = QuatRBT(Cvec3(44, -1.7, -18));
  myPath[147] = QuatRBT(Cvec3(43, -1.7, -12));
  // Curve 38
  myPath[148] = QuatRBT(Cvec3(43, -1.7, -12));
  myPath[149] = QuatRBT(Cvec3(42, -1.7, -6));
  myPath[150] = QuatRBT(Cvec3(35, -1.7, -6));
  myPath[151] = QuatRBT(Cvec3(27, -1.7, -6));
  // Curve 39
  myPath[152] = QuatRBT(Cvec3(27, -1.7, -6));
  myPath[153] = QuatRBT(Cvec3(19, -1.7, -6));
  myPath[154] = QuatRBT(Cvec3(8, -1.7, -5));
  myPath[155] = QuatRBT(Cvec3(4, -1.7, -3));
  return myPath;
}

QuatRBT getBez(double t){
  QuatRBT* myP = getCtrls(4);
  double mT = fmod(t,1);
  int cLoc = 0 + (floor(t) * 4);
  Cvec3 myP0 = myP[cLoc].getTranslation();
  Cvec3 myP1 = myP[cLoc+1].getTranslation();
  Cvec3 myP2 = myP[cLoc+2].getTranslation();
  Cvec3 myP3 = myP[cLoc+3].getTranslation();
  return QuatRBT(Cvec3(
	   (pow((1-mT),3) * myP0[0]) + (3 * mT * pow((1-mT),2) * myP1[0]) + (3 * pow(mT,2) * (1-mT) * myP2[0]) + (pow(mT,3) * myP3[0]),
	   (pow((1-mT),3) * myP0[1]) + (3 * mT * pow((1-mT),2) * myP1[1]) + (3 * pow(mT,2) * (1-mT) * myP2[1]) + (pow(mT,3) * myP3[1]),
	   (pow((1-mT),3) * myP0[2]) + (3 * mT * pow((1-mT),2) * myP1[2]) + (3 * pow(mT,2) * (1-mT) * myP2[2]) + (pow(mT,3) * myP3[2]))
	 );
}
