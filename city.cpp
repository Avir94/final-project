#include "city.h"

Building::Building() {}

Building::Building(QuatRBT xyz, int s) {
  this->xyz = xyz;
  this->s = s;
}

Building* getCity(int size){
  Building myCity[size];
  myCity[0] = Building(QuatRBT(Cvec3(6.35  ,0.3   ,-11  )), 5   );
  myCity[1] = Building(QuatRBT(Cvec3(0     ,0.8   ,-12  )), 7   );
  myCity[2] = Building(QuatRBT(Cvec3(-.5   ,0     ,-1   )), 2   );
  myCity[3] = Building(QuatRBT(Cvec3(15.15 ,-.5   ,1.05 )), 3.75);
  myCity[4] = Building(QuatRBT(Cvec3(13.75 ,-2    ,8.45 )), 2   );
  myCity[5] = Building(QuatRBT(Cvec3(13.75 ,-3    ,10.6 )), 2   );
  myCity[6] = Building(QuatRBT(Cvec3(18    ,-5    ,7.75 )), 3   );
  myCity[7] = Building(QuatRBT(Cvec3(5.75  ,-5    ,1    )), 6   );
  myCity[8] = Building(QuatRBT(Cvec3(17    ,0     ,5    )), 2   );
  myCity[9] = Building(QuatRBT(Cvec3(0.5   ,0     ,2.1  )), 4   );
  myCity[10]= Building(QuatRBT(Cvec3(22    ,-1    ,3.5  )), 1   );
  myCity[11]= Building(QuatRBT(Cvec3(22    ,-1.5  ,2.5  )), 1   );
  myCity[12]= Building(QuatRBT(Cvec3(22    ,-2    ,1.5  )), 1   );
  myCity[13]= Building(QuatRBT(Cvec3(22    ,-2.5  ,0.5  )), 1   );
  myCity[14]= Building(QuatRBT(Cvec3(22    ,-3    ,-0.5 )), 1   );
  myCity[15]= Building(QuatRBT(Cvec3(22    ,-3    ,-1.5 )), 1   );
  myCity[16]= Building(QuatRBT(Cvec3(6.25  ,-4.5  ,-16  )), 5   );
  myCity[17]= Building(QuatRBT(Cvec3(-3.75 ,-2   ,-15.75)), 5   );
  myCity[18]= Building(QuatRBT(Cvec3(-5    ,1     ,-10  )), 3   );
  myCity[19]= Building(QuatRBT(Cvec3(-7    ,-1    ,-9.5 )), 1   );
  myCity[20]= Building(QuatRBT(Cvec3(-8    ,-1    ,-9.5 )), 1   );
  myCity[21]= Building(QuatRBT(Cvec3(-9    ,-1    ,-9.5 )), 1   );
  myCity[22]= Building(QuatRBT(Cvec3(-10   ,-1    ,-9.5 )), 1   );
  myCity[23]= Building(QuatRBT(Cvec3(-11   ,-1    ,-9.5 )), 1   );
  myCity[24]= Building(QuatRBT(Cvec3(-12.5 ,-1    ,-9.75)), 2   );
  myCity[25]= Building(QuatRBT(Cvec3(-12.5 ,-1   ,-11.75)), 2   );
  myCity[26]= Building(QuatRBT(Cvec3(-14.75,-1    ,-9.5 )), 2.75);
  myCity[27]= Building(QuatRBT(Cvec3(-17.25,-1    ,-9.5 )), 2.75);
  myCity[28]= Building(QuatRBT(Cvec3(-20   ,-.5   ,-10  )), 3.5 );
  myCity[29]= Building(QuatRBT(Cvec3(-20   ,-1   ,-13.25)), 3   );
  myCity[30]= Building(QuatRBT(Cvec3(-20   ,-1   ,-16.75)), 3   );
  myCity[31]= Building(QuatRBT(Cvec3(-17   ,-1   ,-17   )), 2   );
  myCity[32]= Building(QuatRBT(Cvec3(-11.25,-1   ,-16.75)), 3   );
  myCity[33]= Building(QuatRBT(Cvec3(-11   ,-1    ,-9.5 )), 1   );
  myCity[34]= Building(QuatRBT(Cvec3(-11   ,-1    ,-9.5 )), 1   );
  myCity[35]= Building(QuatRBT(Cvec3(-11   ,-1    ,-9.5 )), 1   );
  myCity[36]= Building(QuatRBT(Cvec3(-11   ,-1    ,-9.5 )), 1   );
  myCity[37]= Building(QuatRBT(Cvec3(-11   ,-1    ,-9.5 )), 1   );
  myCity[38]= Building(QuatRBT(Cvec3(-11   ,-1    ,-9.5 )), 1   );
  return myCity;
}
  
