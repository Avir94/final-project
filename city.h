#ifndef CITY_H
#define CITY_H

#include "quatrbt.h"

//a structure containing a buildings x,y,z location, and scaled size s
struct Building {
  QuatRBT xyz;
  unsigned int s;
  Building();
  Building(QuatRBT xyz, int s);
};


// return the array of buildings 
Building* getCity(int size);

#endif 
